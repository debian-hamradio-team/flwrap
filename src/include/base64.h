//=====================================================================
//
// base64 encoding / decoding class
//
//=====================================================================

#include <stdio.h> 
#include <stdlib.h> 
#include <ctype.h> 

#include <string>

using namespace std;

typedef unsigned char t_byte;

class base64 {
#define LINELEN 72
private:
	string output;
	size_t iolen;
	size_t iocp;
	bool ateof;
	t_byte dtable[256];
	t_byte etable[256];
	int linelength;
	bool crlf;
	void init();
public:
	base64(bool t = false) {crlf = t; init(); };
	~base64(){};
	std::string encode(std::string in);
	std::string decode(std::string in);
};
