// WRAP - wrap.cxx
//
// Author: Dave Freese, W1HKJ
//         Stelios Bounanos, M0GLD
//
// wrap surrounds critical text with markers and adds a checksum to the
// resulting file.  It unwraps a file that has been previously wrapped.  The
// wrap/unwrap is automatic and the user only need to process the file with
// wrap and it will determine whether a wrap or unwrap is needed.  The wrapped
// file can be transmitted via a noisy channel and then tested at the end site
// for file integrity.  It reports on the success or failure with a small
// dialog box.
//
// wrap is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// WRAP is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with the program; if not, write to the
//
//  Free Software Foundation, Inc.
//  51 Franklin Street, Fifth Floor
//  Boston, MA  02110-1301 USA.
//
// $Id: main.c 141 2008-07-19 15:59:57Z jessekornblum $

#include <config.h>

#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <string.h>
#include <cmath>
#include <stdint.h>

#ifdef __WOE32__
#  include <winsock2.h>
#else
#  include <arpa/inet.h>
#endif

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Enumerations.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Sys_Menu_Bar.H>
#ifdef __WOE32__
#  include <FL/x.H>
#endif
#include <FL/filename.H>

#include "wrapdialog.h"
#include "base64.h"
#include "crc16.h"
#include "lzma/LzmaLib.h"

#ifdef __WOE32__
#  include "wraprc.h"
#endif

int parse_args(int argc, char **argv, int& idx);

using namespace std;

const char *wrap_beg = "[WRAP:beg]";
const char *wrap_end = "[WRAP:end]";
const char *wrap_crlf = "[WRAP:crlf]";
const char *wrap_lf = "[WRAP:lf]";
const char *wrap_chksum = "[WRAP:chksum ";

const char *b64_start = "[b64:start]";
const char *b64_end = "[b64:end]";
const char *wrap_fn = "[WRAP:fn ";

Ccrc16 chksum;
base64 b64(1); // insert lf for ease of viewing

std::string inptext = "";
std::string check = "";
std::string outfilename = "";
std::string inpfilename = "";
std::string inpshortname = "";
std::string outshortname = "";
std::string foldername = "";

std::string errtext = "";
std::string infotext = "";

bool usecompression = false;

void dress(std::string &str)
{
	std::string s = "";
	if (str.empty()) return;
	size_t p = 0;
	std::string rpl = "";
	unsigned char ch;
	for (p = 0; p < str.length(); p++) {
		ch = str[p] & 0xFF;
		rpl = ch;
		if ( ch == '|')
			rpl = "||";
		else if (ch >= 0x80 && ch < 0xC0) {
			rpl = "|-"; rpl += (ch - 0x60);
		}
		else if (ch >= 0xC0) {
			rpl = "|_"; rpl += (ch - 0xA0);
		} 
		else if (ch < ' ') {
			if (ch != 0x09 && ch != 0x0A && ch != 0x0D) {
				rpl = "|+"; rpl += (ch + ' ');
			}
		}
		s.append(rpl);
	}
	str = s;
}

void strip(std::string &s)
{
	if (s.empty()) return;
	size_t p = 0;
	std::string str = "";
	while (p < s.length()) {
		if (s[p] != '|') str += s[p];
		else {
			p++;
			if (s[p] == '|') str += s[p];
			else if (s[p] == '+') { p++; str += (s[p] & 0xFF) - 0x20; }
			else if (s[p] == '-') { p++; str += (s[p] & 0xFF) + 0x60; }
			else if (s[p] == '_') { p++; str += (s[p] & 0xFF) + 0xA0; }
			else p++;
		}
		p++;
	}
	s = str;
}

bool isExtension(const char *s1, const char *s2)
{
#ifdef __WOE32__
	char *sz1 = new char[strlen(s1) + 1];
	char *sz2 = new char[strlen(s2) + 1];
	char *p = 0;
	strcpy(sz1, s1);
	strcpy(sz2, s2);
	for (size_t i = 0; i < strlen(sz1); i++) sz1[i] = toupper(sz1[i]);
	for (size_t i = 0; i < strlen(sz2); i++) sz2[i] = toupper(sz2[i]);
	p = strstr(sz1, sz2);
	delete [] sz1;
	delete [] sz2;
	return (p != 0);
#else
	const char *p = strcasestr(s1, s2);
#endif
	return (p != 0);
}


void base64encode()
{
	std::string outtext;
	outtext = b64.encode(inptext);
	inptext = b64_start;
	inptext.append(outtext);
	inptext.append(b64_end);
}

void convert2crlf(std::string &s)
{
	if (s.find('\r') != std::string::npos) return;
	size_t p = s.find('\n');
	while (p != std::string::npos) {
		s.replace(p, 1, "\r\n");
		p = s.find('\n', p + 2);
	}
}

void convert2lf(std::string &s)
{
	size_t p = s.find("\r\n");

	while (p != std::string::npos) {
		s.replace(p, 2, "\n");
		p = s.find("\r\n");
	}
}

#define LZMA_STR "\1LZMA"

bool compress_always(std::string &input)
{
	// allocate 105% of the original size for the output buffer
	size_t outlen = (size_t)ceil(input.length() * 1.05);
	unsigned char* buf = new unsigned char[outlen];

	size_t plen = LZMA_PROPS_SIZE;
	unsigned char outprops[LZMA_PROPS_SIZE];
	uint32_t origlen = htonl(input.length());
	size_t inlen = input.length();

	int r;
	char msg[200];
	if ((r = LzmaCompress(buf, &outlen, (const unsigned char*)input.data(), input.length(),
			      outprops, &plen, 9, 0, -1, -1, -1, -1, -1)) != SZ_OK) {
		snprintf(msg, sizeof(msg), "File cannot be compressed.\nReprocess with compression disabled");
		errtext = msg;
		return false;
	}
	// write LZMA_STR + original size (in network byte order) + props + data
	input.reserve(strlen(LZMA_STR) + sizeof(origlen) + sizeof(outprops) + outlen);
	input.assign(LZMA_STR);
	input.append((const char*)&origlen, sizeof(origlen));
	input.append((const char*)&outprops, sizeof(outprops));
	input.append((const char*)buf, outlen);
	snprintf(msg, sizeof(msg), "Compress: in = %ld, out = %ld", (long)inlen, (long)outlen);
	infotext = msg;
	delete [] buf;
	return true;
}

void decompress_maybe(std::string& input)
{
	// input is LZMA_STR + original size (in network byte order) + props + data
	if (input.find(LZMA_STR)) {
		errtext.clear();
		return;
	}

	const char* in = input.data();
	size_t outlen = ntohl(*reinterpret_cast<const uint32_t*>(in + strlen(LZMA_STR)));
	// if (outlen > 1 << 25) {
	// 	cerr << "W: refusing to decompress data (> 32MiB)\n";
	// 	return;
	// }
	unsigned char* buf = new unsigned char[outlen];

	unsigned char inprops[LZMA_PROPS_SIZE];
	memcpy(inprops, in + strlen(LZMA_STR) + sizeof(uint32_t), LZMA_PROPS_SIZE);

	size_t inlen = input.length() - strlen(LZMA_STR) - sizeof(uint32_t) - LZMA_PROPS_SIZE;

	int r;
	char msg[200];
	if ((r = LzmaUncompress(buf, &outlen, (const unsigned char*)in + input.length() - inlen, &inlen,
				inprops, LZMA_PROPS_SIZE)) != SZ_OK) {
		snprintf(msg, sizeof(msg), "Lzma Decompress failed with error code %d\n", r);
		errtext = msg;
	} else {
		input.reserve(outlen);
		input.assign((const char*)buf, outlen);
	}

	delete [] buf;
}

bool wrapfile(bool isbinary)
{
	bool iscrlf = false;
	outfilename.append(".wrap");
	outshortname = fl_filename_name(outfilename.c_str());

#ifdef ____WOE32____
	int  insize = inptext.length();
#else
	long insize = inptext.length();
#endif

	dress(inptext);

	if (isbinary) {
		if (!compress_always(inptext)) return false;
		base64encode();
	} else {
		if (inptext.find("\r\n") != std::string::npos) {
			iscrlf = true;
			convert2lf(inptext); // checksum & data transfer always based on Unix end-of-line char
		}
	}

	std::string originalfilename = wrap_fn;
	originalfilename.append(inpshortname);
	originalfilename += ']';

	inptext.insert(0, originalfilename);

	check = chksum.scrc16(inptext);

	ofstream wrapstream(outfilename.c_str(), ios::binary);
	if (wrapstream) {
		wrapstream << wrap_beg << (iscrlf ? wrap_crlf : wrap_lf) << inptext << wrap_chksum << check << ']' << wrap_end;
		wrapstream.close();
	} else {
		errtext = "Cannot open output file";
		return false;
	}
	char msg[200];

#ifdef ____WOE32____
	int  outsize =
#else
	long outsize =
#endif
	strlen(wrap_beg) + (iscrlf ? strlen(wrap_crlf) : strlen(wrap_lf)) +
			inptext.length() + strlen(wrap_chksum) + check.length() + 1 + strlen(wrap_end);

#ifdef ____WOE32____
	snprintf(msg, sizeof(msg), "In = %d bytes, Out = %d bytes", insize, outsize);
#else
	snprintf(msg, sizeof(msg), "In = %ld bytes, Out = %ld bytes", insize, outsize);
#endif
	infotext = msg;

	return true;
}

// all of this crlf checking is necessary because the original file may have had crlf pairs
// or the OS save may have inserted them into the text (MSDOS) har!
bool unwrapfile()
{
	size_t p1, p2, p3, p4;
	std::string wtext;
	std::string testsum;
	std::string inpsum;
	bool iscrlf = false;

	p1 = inptext.find(wrap_beg);
	if (p1 == std::string::npos) {
		errtext = "Not a wrap file";
		return false;
	}
	p1 = inptext.find(wrap_crlf);
	if (p1 != std::string::npos) { // original text had crlf pairs
		iscrlf = true;
		p1 += strlen(wrap_crlf);
	} else {
		p1 = inptext.find(wrap_lf);
		if (p1 == std::string::npos) {
			errtext = "Invalid file";
			return false;
		}
		p1 += strlen(wrap_lf);
	}

	p2 = inptext.find(wrap_chksum, p1);
	if (p2 == std::string::npos) return false;
	wtext = inptext.substr(p1, p2-p1); // this is the original document

	p3 = p2 + strlen(wrap_chksum);
	p4 = inptext.find(']', p3);
	if (p4 == std::string::npos) {
		errtext = "Cannot find checksum in file";
		return false;
	}
	inpsum = inptext.substr(p3, p4-p3);
	p4 = inptext.find(wrap_end, p4);
	if (p4 == std::string::npos) {
		errtext = "No end tag in file";
		return false;
	}

	if (wtext.find("\r\n") != std::string::npos)
		convert2lf(wtext);

	testsum = chksum.scrc16(wtext);

	if (inpsum != testsum) {
		errtext = "Checksum failed\n";
		errtext.append(inpsum);
		errtext.append(" in file\n");
		errtext.append(testsum);
		errtext.append(" computed");
		return false;
	}

	if (wtext.find(wrap_fn) == 0) {
		size_t p = wtext.find(']');
		outshortname = wtext.substr(0, p);
		outshortname.erase(0, strlen(wrap_fn));
		wtext.erase(0,p+1);
// check for protocol abuse
bool t1 = (outshortname.find('/') != std::string::npos);
bool t2 = (outshortname.find('\\') != std::string::npos);
bool t3 = (outshortname == ".");
bool t4 = (outshortname == "..");
bool t5 = (outshortname.find(":") != std::string::npos);
bool t6 = outshortname.empty();
		if (t1 || t2 || t3 || t4 || t5 || t6) {
			errtext = "Filename corrupt, possible protocol abuse\n";
			if (t1) errtext.append("Filename contains '/' character\n");
			if (t2) errtext.append("Filename contains '\\' character\n");
			if (t3) errtext.append("Filename contains leading '.' character\n");
			if (t4) errtext.append("Filename contains leading '..' std::string\n");
			if (t5) errtext.append("Filename contains MS directory specifier\n");
			if (t6) errtext.append("Filename is empty\n");
			return false;
		}
		outfilename = foldername;
		outfilename.append(outshortname);
	} else {
		p1 = outfilename.find(".wrap");
		if (p1 != std::string::npos)
			outfilename.erase(p1);
	}

	if (iscrlf)
		convert2crlf(wtext);

	p1 = wtext.find(b64_start);
	if (p1 != std::string::npos) {
		p1 += strlen(b64_start);
		p2 = wtext.find(b64_end, p1);
		if (p2 == std::string::npos) {
			errtext = "Base 64 decode failed";
			return false;
		}
		wtext = b64.decode(wtext.substr(p1, p2-p1));
		decompress_maybe(wtext);
	}

	strip(wtext);

	ofstream tfile(outfilename.c_str(), ios::binary);
	if (tfile) {
		tfile << wtext;
		tfile.close();
	} else {
		errtext = "Cannot open output file";
		return false;
	}
	return true;
}

bool readfile(bool& isbinary)
{
	char c;
	ifstream textfile;
	textfile.open(inpfilename.c_str(), ios::binary);
	if (!textfile)
		return false;

	inptext.clear();
	textfile.seekg(0, ios::end);
	inptext.reserve(textfile.tellg());
	textfile.seekg(0, ios::beg);

	isbinary = false;
	while (textfile.get(c))
		inptext += c;

	return true;
}

void cb_close()
{
	exit(0);
}

void wrap_unwrap()
{
	char result[1000];
	inpshortname = fl_filename_name(inpfilename.c_str());
	foldername = inpfilename;
	foldername.erase(foldername.find(inpshortname));

	txtResult->value("");
	txtResult->redraw();
	infotext = "";

	bool isbinary = false;
	if (inpfilename.empty() || !readfile(isbinary)) {
		snprintf(result, sizeof(result),
"Usage: flwrap [--usecomp] FILENAME or\n\
        (1) Drag & Drop File onto desktop icon\n\
        (2) Drag & Drop File into drop box ==>");
	} else {
		if (usecompression) isbinary = true;

		if (inptext.find("[WRAP:") == std::string::npos) {
			if (!wrapfile(isbinary))
				snprintf(result, sizeof(result), "%s\n", errtext.c_str());
			else {
				snprintf(result, sizeof(result),
				"Success\ninput: %s\noutput: %s\nFolder: %s\n%s",
					inpshortname.c_str(),
					outshortname.c_str(),
					foldername.c_str(),
					infotext.c_str() );
			}
		} else {
			if (!unwrapfile())
				snprintf(result, sizeof(result),
					"%s", errtext.c_str());
			else {
				outshortname = fl_filename_name(outfilename.c_str());
				snprintf(result, sizeof(result),
					"Success\ninput: %s\noutput: %s\nFolder: %s\n%s",
					inpshortname.c_str(),
					outshortname.c_str(),
					foldername.c_str(),
					infotext.c_str() );
			}
		}
	}
	inpDropBox->value("");
	txtResult->value(result);
}

int main(int argc, char **argv)
{
	Fl_Window *mainwin = make_window();
	std::string Title = "Flwrap ";
	Title.append(VERSION);
	mainwin->label(Title.c_str());

	std::string argvs = "";
	for (int i = 0; i < argc; i++) argvs.append(argv[i]).append(" ");

	int arg_idx;
	argc = Fl::args(argc, argv, arg_idx, parse_args);

	wrap_unwrap();

#ifdef __WOE32__
	mainwin->icon((char*)LoadIcon(fl_display, MAKEINTRESOURCE(IDI_ICON)));
	mainwin->show(1, argv);
#else
	mainwin->show(argc, argv);
#endif
	return Fl::run();
}

int parse_args(int argc, char **argv, int& idx)
{
	if (strcasecmp("--help", argv[idx]) == 0) {
		printf("\
  --help this help text\n\
  --version\n\
\n\
  Usage: flwrap [--usecomp] FILENAME or\n\
         Drag & Drop File onto desktop icon\n");
			exit(0);
		}
	if (strcasecmp("--version", argv[idx]) == 0) {
		printf("Version: " VERSION "\n");
		exit(0);
	}

	if (strcasecmp("--usecomp", argv[idx]) == 0) {
		usecompression = true;
		idx++;
		return 1;
	}

// OS X passes process number as a command line argument
	if (strncasecmp("-psn", argv[idx], 4) == 0) {
		idx++;
		return 1;
	}

	inpfilename = outfilename = argv[idx];
	idx++;
	return 1;
}

void inpDropBox_changed()
{
	std::string fname;
	size_t n = std::string::npos;
	fname = inpDropBox->value();
	if ((n = fname.find("file:///")) != std::string::npos)
		fname.erase(n, 7);
	while ((n = fname.find('\n')) != std::string::npos)
		fname.erase(n, 1);
	while ((n = fname.find('\r')) != std::string::npos)
		fname.erase(n, 1);
	inpDropBox->value(fname.c_str());
	inpfilename = outfilename = fname;
	wrap_unwrap();
}
